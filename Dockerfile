FROM alpine
ARG ARCH
RUN echo "Building for $ARCH"
ARG XUI_VERSION=v2.2.1
ENV TZ=GMT+0
WORKDIR /app
RUN apk add --no-cache --update \
    ca-certificates \
    tzdata \
    fail2ban \
    bash
RUN wget https://raw.githubusercontent.com/mhsanaei/3x-ui/${XUI_VERSION}/DockerEntrypoint.sh && chmod +x /app/DockerEntrypoint.sh
RUN wget https://raw.githubusercontent.com/mhsanaei/3x-ui/${XUI_VERSION}/x-ui.sh -O /usr/bin/x-ui && chmod +x /usr/bin/x-ui
RUN wget https://github.com/MHSanaei/3x-ui/releases/download/${XUI_VERSION}/x-ui-linux-${ARCH}.tar.gz
